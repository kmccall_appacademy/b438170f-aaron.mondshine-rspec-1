def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(num_arr)
  num_arr.empty? ? 0 : num_arr.reduce(:+)
end

def multiply(num_arr)
  num_arr.empty? ? 0 : num_arr.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  num == 0 ? 0 : (1..num).to_a.reduce(:*)
end
