def ftoc(temperature_in_f)
  (temperature_in_f - 32) * (5.0 / 9.0)
end

def ctof(temperature_in_c)
  temperature_in_c * 9.0 / 5.0 + 32
end
