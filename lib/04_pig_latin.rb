def translate(str)
  str_arr = str.split
  str_arr.map!.each { |word| transform(word) }
  str_arr.join(" ")

end

def transform(word)
  vowels = 'aeiou'
  phoneme = 'qu'

  if vowels.include?(word[0].downcase)
    word + 'ay'
  elsif word.include?(phoneme)
    first_phoneme = word.downcase.index(phoneme) + 2
    word[first_phoneme..-1] + word[0..first_phoneme - 1] + 'ay'
  else
    first_vowel = word.index(/[aeiou]/)
    word[first_vowel..-1] + word[0..first_vowel - 1] + 'ay'
  end
end
