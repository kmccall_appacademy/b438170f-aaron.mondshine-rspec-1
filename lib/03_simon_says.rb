def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, repeater = 2)
  new_word_arr = []
  repeater.times { new_word_arr << str }
  new_word_arr.join(" ")
end

def start_of_word(str, idx)
  str[0..idx - 1]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  str_arr = str.split
  str_arr[0].capitalize!
  l_w = ['and', 'over', 'the']
  str_arr[1..-1].map!.each { |w| l_w.include?(w) ? w : w.capitalize! }
  str_arr.join(" ")
end
